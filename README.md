# Semir Test Assign #



### What is this repository for? ###

Make a game of city collapse based on the given area, for a certain period of time.

The area, the list of cities, the list of exact cities, and the time (in seconds) needed for the successful completion of the game are loaded from json (data.json).

There should be two .html pages.

On the first page make to display the area, time as well as the input field in which cities are entered (with auto complete option) that are in JSON. When a user chooses rec and clicks on "add", the city appears as selected in the city list (see "page 1") In the list there is an option to delete cities. When the time expires or is clicked at the end, it goes to the other side where it shows how many correct cities have been marked in the previous step (out of the total number of selected cities) and the chart in which the percentage is shown (see "page 2"). to start from 0% and slowly increase until it reaches the desired percentage.

* Version 1.0

### How do I get set up? ###

* npm install
* ng serve
* 
* Used angular cli for raising the project and creating components, services, etc.

