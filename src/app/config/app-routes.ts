import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameComponent } from '../modules/game/components/game/game.component';
import { ScoreComponent } from '../modules/game/components/score/score.component';
import { PageNotFoundComponent } from '../modules/game/components/page-not-found/page-not-found.component';

export const AppRoutes: Routes = [
  {path: '', redirectTo: 'play', pathMatch: 'full'},
  {path: 'play', component: GameComponent},
  {path: 'score', component: ScoreComponent},
  {path: '**', component: PageNotFoundComponent}
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(AppRoutes, {enableTracing: false});
