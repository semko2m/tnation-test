import { Component, OnDestroy, OnInit } from '@angular/core';
import { CitiesService } from '../../services/cities.service';
import { Router } from '@angular/router';
import { ScoreService } from '../../services/score.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, OnDestroy {

  cities = [];
  preSelected = '';
  selectedCities = [];
  correctAnswers = [];
  correctAnswersCount = 0;
  minutes = 0;
  seconds = 0;
  area = '';
  private timer;
  private totalSeconds;
  private log = false;

  constructor( private citiesService: CitiesService,
               private router: Router,
               private scoreService: ScoreService ) {
  }

  ngOnInit() {
    this.citiesService.getCities().subscribe(res => {
      if (this.log) {
        console.log('JSON data:', res);
      }
      this.cities = res.ponudjene;
      this.correctAnswers = res.tacno;
      this.totalSeconds = res.vreme;
      this.area = res.oblast;
      this.initiateTimer();
    });
  }

  ngOnDestroy() {
    this.preSelected = '';
    this.area = '';
    this.cities = this.selectedCities = [];
    this.timer = this.totalSeconds = null;
    this.minutes = this.seconds = 0;
  }

  addToList( item ) {

    // Check if item is valid string and item not exists in selected cities
    if (item !== '' && this.selectedCities.indexOf(item) === -1) {
      this.selectedCities.push(item);
      this.preSelected = '';
    }
  }

  removeFromList( item ) {
    this.selectedCities.splice(this.selectedCities.indexOf(item), 1);
  }

  initiateTimer() {
    if (this.timer) {
      clearInterval(this.timer);
    }

    this.timer = setInterval(() => {
      this.totalSeconds--;

      if (this.totalSeconds === 0) {
        this.finishGame();
        clearInterval(this.timer);
      }
      this.convertToDisplay();
    }, 1000);

  }

  convertToDisplay() {
    this.minutes = Math.floor(this.totalSeconds / 60);
    this.seconds = this.totalSeconds - this.minutes * 60;
  }

  submitGame() {
    if (this.selectedCities.length === 0) {
      alert('Molimo dodajte gradove!');
      return;
    }
    this.finishGame();
  }

  finishGame() {
    this.countResults();
    const score = this.finalScore(this.correctAnswersCount, this.correctAnswers.length);

    if (this.log) {
      console.log('Igra je završena. Ukupno tačnih: ', score);
    }

    this.scoreService.score = score;
    console.log('before router', this.scoreService.score);
    this.router.navigate(['/score']);
  }

  finalScore( noOfCorrectAnswer, noOfTotalAnsfer ) {
    return noOfCorrectAnswer / noOfTotalAnsfer * 100;
  }

  countResults() {
    this.correctAnswersCount = 0;
    this.correctAnswers.forEach(answer => {
      this.selectedCities.forEach(city => {
        if (answer.toLowerCase() === city.toLowerCase()) {
          this.correctAnswersCount++;
        }
      });
    });
  }

}
