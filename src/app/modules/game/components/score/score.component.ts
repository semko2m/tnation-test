import {Component, OnDestroy, OnInit} from '@angular/core';
import {ScoreService} from '../../services/score.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss']
})
export class ScoreComponent implements OnInit {

  score;

  constructor(private router: Router,
              private scoreService: ScoreService) {
  }

  ngOnInit() {
    if (this.scoreService.score == null) {
      this.router.navigate(['']);
    }
    this.score = 0;
    setTimeout(() => {
      this.uvecaj();
    }, 1000);
  }

  /**
   * Uvećaj score nakon loada. Da bi dobili željeni efekat koji je definiran u zadatku
   * Chart treba da krene od 0% i polako uvecava sve dok ne stigne do zeljenog procenta
   */
  uvecaj() {
    this.score = this.scoreService.score;
  }

}
