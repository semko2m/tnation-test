import { ModuleWithProviders, NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { ProgressbarModule, TypeaheadModule } from 'ngx-bootstrap';

import { CitiesService } from './services/cities.service';
import { ScoreService } from './services/score.service';
import { GameComponent } from './components/game/game.component';
import { ScoreComponent } from './components/score/score.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    GameComponent,
    ScoreComponent,
    PageNotFoundComponent
  ],
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    TypeaheadModule.forRoot(),
    ProgressbarModule.forRoot(),
    RouterModule,
  ],
  exports: [
    GameComponent,
  ]
})
export class GameModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: GameModule,
      providers: [CitiesService, ScoreService]
    };
  }
}
